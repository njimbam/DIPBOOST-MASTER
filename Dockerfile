# FROM node:latest AS build

# WORKDIR /build
# COPY package.json package-lock.json ./
# # COPY package.json package.json
# # COPY package-lock.json package-lock.json
# RUN npm ci
# COPY public/ public
# COPY src/ src
# RUN npm run build

# FROM nginx:alpine
# WORKDIR /var/www/html
# COPY --from=build /build/build/ .


FROM node:latest AS build
WORKDIR /app
COPY package.json package-lock.json ./
# COPY package.json package.json
# COPY package-lock.json package-lock.json
RUN npm ci
COPY . .
RUN npm run build

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=build /app/build/ .
ENTRYPOINT ["nginx", "-g", "daemon off;"]